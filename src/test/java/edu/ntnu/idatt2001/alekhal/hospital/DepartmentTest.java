package edu.ntnu.idatt2001.alekhal.hospital;

import edu.ntnu.idatt2001.alekhal.hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    @Nested
    class RemoveTest {

        Employee alek;
        Patient tor;
        Employee ola;
        Department department;

        @BeforeEach
        void setup(){
            alek = new Employee("Aleksander", "Holthe", "");
            tor = new Patient("Tor-Øyvind", "Bakken", "");
            ola = new Employee("Ola", "Olason", "");
            department = new Department("Test department");
            department.addEmployee(alek);
            department.addPatient(tor);
        }

        @Test
        void removePatient() throws RemoveException {
            assertDoesNotThrow(() -> department.remove(alek));
        }

        @Test
        void removeEmployee() throws RemoveException{
            assertDoesNotThrow(() -> department.remove(tor));
        }

        @Test
        void removeNonExistingPerson() throws RemoveException{
            assertThrows(RemoveException.class, () -> department.remove(ola));
        }

        @Test
        void removeNull() throws RemoveException{
            assertThrows(RemoveException.class, () -> department.remove(null));
        }
    }
}