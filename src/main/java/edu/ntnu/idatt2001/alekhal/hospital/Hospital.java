package edu.ntnu.idatt2001.alekhal.hospital;

import java.util.ArrayList;

/**
 * The hospital class represents a hospital.
 * It contains a list of departments and it can add more of them.
 */

public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments(){
        return departments;
    }

    public void addDepartment(Department department){
        departments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
