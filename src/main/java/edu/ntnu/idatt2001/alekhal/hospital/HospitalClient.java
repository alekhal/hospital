package edu.ntnu.idatt2001.alekhal.hospital;

import edu.ntnu.idatt2001.alekhal.hospital.exception.RemoveException;

import java.util.ArrayList;

/**
 * Client test class that adds a hospital, fills it with data and then deletes an employee and a non-existing patient.
 * Since the patient does not exist within the department there is a try-catch block to make sure that the error gets handled without terminating
 * the program.
 */

public class HospitalClient {

    public static void main(String[] args) throws RemoveException {
        Hospital stOlavs = new Hospital("St. Olavs");
        HospitalTestData.fillRegisterWithTestData(stOlavs);

        ArrayList<Department> departments = stOlavs.getDepartments();
        stOlavs.getDepartments().get(0).getEmployees().remove(new Employee("Odd Even", "Primtallet", ""));

        try{
            stOlavs.getDepartments().get(1).remove(new Patient("Odd Even", "Primtallet", ""));
        }catch (RemoveException r){
            System.out.println("Could not find patient.");
        }
    }
}