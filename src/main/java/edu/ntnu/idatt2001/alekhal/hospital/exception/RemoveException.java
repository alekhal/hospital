package edu.ntnu.idatt2001.alekhal.hospital.exception;

public class RemoveException extends Exception{
    static final long serialVersionUID = 1L;

    public RemoveException(String message){
        super(message);
    }
}
