package edu.ntnu.idatt2001.alekhal.hospital.healthpersonnel.doctor;

import edu.ntnu.idatt2001.alekhal.hospital.Patient;

/**
 * Class for Surgeon. This class can set diagnoses on patients because it inherits the Doctor class.
 */

public class Surgeon extends Doctor{

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
