package edu.ntnu.idatt2001.alekhal.hospital;

import edu.ntnu.idatt2001.alekhal.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The Department class represents one department under a hospital.
 * It has a list of all employees and a list of all patients within the department.
 * It can add or remove employees/patients.
 */

public class Department {

    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    public Department(String departmentName){
        this.departmentName = departmentName;
        employees = new ArrayList<Employee>();
        patients = new ArrayList<Patient>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Adds employee as long as the amployee is not already registered.
     */

    public void addEmployee(Employee employee){
        if(!employees.contains(employee)){
            employees.add(employee);
        }
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     * Adds patient as long as the paitient is not already registered.
     * */

    public void addPatient(Patient patient){
        if(!patients.contains(patient)){
            patients.add(patient);
        }
    }

    /**
     * Removes an employee or patient from the department. This method throws an error if the person does not
     * exist within the department. In this method i could have used instanceOf.
     * @param person The person that will be removed.
     * @throws RemoveException if the person does not exist within the department.
     */

    public void remove(Person person) throws RemoveException {
        if(employees.remove(person)) {

        }else if(patients.remove(person)){

        }else{
            throw new RemoveException("Cannot find user");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) &&
                employees.equals(that.employees) &&
                patients.equals(that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}
