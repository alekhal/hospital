package edu.ntnu.idatt2001.alekhal.hospital;

/**
 * Classes implementing this interface has the possibility for a Doctor to set a diagnosis.
 */

public interface Diagnosable {

    public void setDiagnosis(String diagnosis);
}
