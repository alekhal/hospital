package edu.ntnu.idatt2001.alekhal.hospital.healthpersonnel.doctor;

import edu.ntnu.idatt2001.alekhal.hospital.Employee;
import edu.ntnu.idatt2001.alekhal.hospital.Patient;

/**
 * Base class for the different kinds of doctors.
 * Only Classes inheriting this class can set diagnoses on patients.
 */

public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);

    @Override
    public String toString() {
        return super.toString();
    }
}