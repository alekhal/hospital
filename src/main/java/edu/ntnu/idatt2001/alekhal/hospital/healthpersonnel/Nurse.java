package edu.ntnu.idatt2001.alekhal.hospital.healthpersonnel;

import edu.ntnu.idatt2001.alekhal.hospital.Employee;

/**
 * Class representing a nurse. This class does not have permission to set a diagnosis or change patient info in any way.
 */

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
