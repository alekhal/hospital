Meta document explaining some of my code.

I originally used composition for my code, but because the class diagram uses aggregation
(i.e addEmployee(Employee) and addPatient(Patient) in the Department class) i figured that everything should use aggregation.
For example i made getDepartments in the Hospital class to originally look like:

    public ArrayList<Department> getDepartments(){
        ArrayList<Department> temporary = new ArrayList<>();
        for(Department department:departments){
            temporary.add(department);
        }
        return temporary;
    }

I changed this to:

    public ArrayList<Department> getDepartments(){
        return departments;
    }

to keep the code consistent and as close to the class diagram as possible.


I also changed the package names a bit to better reflect my own organization system to make sure that i have the project
in the right spot in case i need to look back at it.

Lastly i made the choice to not comment most things using javadoc because i feel that most of the code is pretty self explanatory.